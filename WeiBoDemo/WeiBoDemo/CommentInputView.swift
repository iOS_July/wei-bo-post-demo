//
//  CommentInputView.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/26.
//

import SwiftUI

struct CommentInputView: View {
    
    let post: Post
    
    @State private var text: String = ""
    @State private var showEmptyTextHUD: Bool = false
    
    // 利用系统提供 存储在Environment \. 里的值(esc查看)，来声明变量，控制模态关闭
    @Environment(\.presentationMode) var presentationMode
    
    // 因为点击发送之后要修改评论数，所以也要用到环境变量UserData
    @EnvironmentObject var userData: UserData
    
    @ObservedObject private var keyboardResponder = KeyboardResponder()
    
    var body: some View {
        
        VStack (spacing: 0) {
            CommentTextView(text: $text, beginEditingOnAppear: true)
            
            HStack (spacing: 0) {
                
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }, label: {
                    Text("取消")
                        .padding()
                })
                
                Spacer()
                
                Button(action: {
                    // 这里是将空格、return换行也给判断进去了
                    if self.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                        self.showEmptyTextHUD = true
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            self.showEmptyTextHUD = false
                        }
                        return
                    }
                    print(self.text)
                    // 因为self.post属性，不可修改，所以这里写一个局部变量，来传递
                    var post = self.post
                    post.commentCount += 1
                    self.userData.update(post)
                    self.presentationMode.wrappedValue.dismiss()
                    
                }, label: {
                    Text("发送")
                        .padding()
                })
            }
            .font(.system(size: 18))
            .foregroundColor(.black)
            
        }
        .overlay(
            Text("评论不能为空！")
                .scaleEffect(showEmptyTextHUD ? 1 : 0.5)
                .animation(.spring(dampingFraction: 0.5))//scaleEffect的动画,回弹效果为0.5
                
                .opacity(showEmptyTextHUD ? 1 : 0)
                .animation(.easeInOut)//opacity的动画
        )
        .padding(.bottom, keyboardResponder.keyboardHeight)
        .edgesIgnoringSafeArea(keyboardResponder.keyboardShow ? .bottom : [])
        
        
    }
}

struct CommentInputView_Previews: PreviewProvider {
    static var previews: some View {
        CommentInputView(post: UserData().recomendPostList.list[0])
    }
}
