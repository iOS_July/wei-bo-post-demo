//
//  PostListView.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/23.
//

import SwiftUI
import BBSwiftUIKit

struct PostListView: View {
    
    let category: PostListCategory
    
    @EnvironmentObject var userData: UserData
    
    var postList: PostList {
        switch category {
        case .recomend:
            return loadPostListData("PostListData_recommend_1.json")
        case .hot:
            return loadPostListData("PostListData_hot_1.json")
        }
    }
    
    var body: some View {
        
        BBTableView(userData.postList(for: category).list) { post in
            NavigationLink(destination: PostDetailView(post: post),
                           label: {
                            PostCell(post: post)
                           })
                .buttonStyle(OriginalButtonStyle())
        }
        .bb_setupRefreshControl({ control in
            //自定义刷新样式
            control.tintColor = .blue
            control.attributedTitle = NSAttributedString(string: "正在加载...", attributes: [.foregroundColor: UIColor.blue])
        })
        .bb_pullDownToRefresh(isRefreshing: $userData.isRefreshing) {
            print("下拉刷新")
            
            self.userData.refreshPostLost(for: self.category)
            /*
            // 这里测试一下错误信息
            self.userData.loadingError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "你猜什么错误"])

            DispatchQueue.main.asyncAfter(deadline: .now()+1) {

                self.userData.isRefreshing = false
                self.userData.loadingError = nil
            }
         */
        }
        .bb_pullUpToLoadMore(bottomSpace: 30) {
            print("上拉刷新")
            self.userData.loadMorePostList(for: category)
            
            /*
             if self.userData.isLoadingMore { return }
             self.userData.isLoadingMore = true
            // 数据获取完成
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                
                self.userData.isLoadingMore = false
            }
             */
        }
        .overlay(
            Text(userData.loadingErrorText)
                .bold()
                .frame(width: 200)
                .padding()
                .background(
                    RoundedRectangle(cornerRadius: 10)
                        .foregroundColor(.white)
                        .opacity(0.8)
                )
                .animation(nil)
                .scaleEffect(userData.showLoadingError ? 1 : 0.5)
                .animation(.spring(dampingFraction: 0.5))//scaleEffect的动画,回弹效果为0.5
                
                .opacity(userData.showLoadingError ? 1 : 0)
                .animation(.easeInOut)//opacity的动画
        )
            
        /*
        List {
            ForEach(postList.list, id: \.id) { post in
                
                ZStack {
                    PostCell(post: post)
                    NavigationLink(destination: PostDetailView(post: post),
                        label: {
                            EmptyView()
                        })
                        .hidden()
                }
                .listRowInsets(EdgeInsets())
                .frame(width: ScreenWidth)
                
                
                
            }
        }
        */
        
        /*
        // 这样加了导航，有个问题，右侧会有一个小箭头
        List {
            ForEach(postList.list, id: \.id) { post in
                
                NavigationLink(
                    destination: Text("Destination"),
                    label: {
                        PostCell(post: post)
                    })
                    .listRowInsets(EdgeInsets())
                
                
            }
        }
        */
        
        /*
         
         // 用一个数组，来初始化 List，数组中的元素会被遍历，每一次取出一个元素 post ，也就是in前面那个，然后执行 闭包里的指令
        List(postList.list, id: \.id) { post in
            // 这里是根据一个 post 生成一个 PostCell
            PostCell(post: post)
            //这里消除不了整体往右偏的bug，需要用ForEach来循环才行
                .listRowInsets(EdgeInsets())
            
        }
        
        */
    }
}

struct PostListView_Previews: PreviewProvider {
    static var previews: some View {
        
        NavigationView {
            PostListView(category: .recomend)
                .navigationTitle("标题")
                .navigationBarHidden(true)
        }
        .environmentObject(UserData())
        
    }
}
