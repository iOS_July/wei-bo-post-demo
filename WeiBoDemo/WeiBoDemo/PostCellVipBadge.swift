//
//  PostCellVipBadge.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/23.
//

import SwiftUI

struct PostCellVipBadge: View {
    
    let vip: Bool
    var body: some View {
        // group 包裹之后，当vip为否时，为空也能正常编译
        Group {
            if vip {
                
                Text("V")
                    .bold()
                    .font(.system(size: 11))
                    .frame(width: 15, height: 15)
                    .foregroundColor(.yellow)
                    .background(Color.red)
                    .clipShape(Circle())
                    .overlay(
                        RoundedRectangle(cornerRadius: 15/2)
                            //只需要边框，描边
                            .stroke(Color.white, lineWidth: 1)
                    )
                
            }
            
            
        }
        
    }
}

struct PostCellVipBadge_Previews: PreviewProvider {
    static var previews: some View {
        PostCellVipBadge(vip: true)
    }
}
