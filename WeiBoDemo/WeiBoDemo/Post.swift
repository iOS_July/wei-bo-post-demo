//
//  Post.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/23.
//

import SwiftUI
import SDWebImageSwiftUI

//Data Model
struct Post: Codable {//Codable 可编码、可解码
    /// 头像
    let avatar: String
    /// 是否是Vip
    let vip: Bool
    /// 姓名
    let name: String
    /// 日期
    let date: String
    /// 是否关注
    var isFollowed: Bool
    
    /// id
    let id: Int
    /// 文字内容
    let text: String
    /// 图片
    let images: [String]
    
    /// 评论数
    var commentCount: Int
    /// 点赞数
    var likeCount: Int
    /// 是否点赞
    var isLiked: Bool
    
    
}


struct PostList: Codable {
    var list: [Post]
    
}

//MARK: - 对model数据的一些扩展，可以用来做一些纯数据以外的事情
extension Post {
    
    /// 评论按钮显示文字 -- 根据评论数来计算的 -- 只读属性(计算属性)
    var commentCountText: String {
        if commentCount <= 0 { return "评论" }
        if commentCount <= 1000 { return "\(commentCount)" }
        return String(format: "%.1fk", Double(commentCount) / 1000 )
    }
    /// 点赞按钮显示文字 -- 根据点赞数来计算的 -- 只读属性(计算属性)
    var likeCountText: String {
        if likeCount <= 0 { return "点赞" }
        if likeCount <= 1000 { return "\(likeCount)" }
        return String(format: "%.1fk", Double(likeCount) / 1000 )
    }
    
    //同理，头像也可以做这样的处理
    var avatarImage: WebImage {
        return loadImage(name: avatar)
    }
    
}
extension Post: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id
    }
}

/// 信息model 数组 、全局变量
//let postList = loadPostListData("PostListData_recommend_1.json") // 用了环境变量UserData了，这里就去掉了

/// 屏幕宽度
let ScreenWidth = UIScreen.main.bounds.width
/// 屏幕高度
let ScreenHeight = UIScreen.main.bounds.height

/// 加载信息列表
/// - Parameter fileName: 本地加载文件名
/// - Returns: Post模型数组PostList
func loadPostListData(_ fileName: String) -> PostList {
    guard let url = Bundle.main.url(forResource: fileName, withExtension: nil) else {
        fatalError("Can not find \(fileName) in main bundle")
    }
    guard let data = try? Data(contentsOf: url) else {
        fatalError("Can not load \(url)")
    }
    
    guard let list = try? JSONDecoder().decode(PostList.self, from: data) else {
        fatalError("Can not parse post list json data")
    }
    
    return list
}


/// 加载网络图片
/// - Parameter name: 图片名称
/// - Returns: Image()
func loadImage(name: String) -> WebImage {
    // 加载网络图片
    return WebImage(url: URL(string: NetworkApiBaseURL + name))
        .placeholder { Color.gray }
}
/// 加载本地图片
/// - Parameter name: 图片名称
/// - Returns: Image()
//func loadImage(name: String) -> Image {
//    return Image(uiImage: UIImage(named: name)!)
//}
