//
//  WebImageDemo.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/30.
//

import SwiftUI
import SDWebImageSwiftUI

struct WebImageDemo: View {
    
    let url: URL?
    
    var body: some View {
        
        WebImage(url: url)
            .placeholder{ Color.gray }
            .resizable()
            .onSuccess(perform: { (_, _, _) in
                print("Success")
                // 图片获取成功后，清楚缓存
                SDWebImageManager.shared.imageCache.clear(with: .all, completion: nil)
            })
            .onFailure(perform: { _ in
                print("Failure")
            })
            .scaledToFill()
            .frame(height: 500)
            .clipped()
    }
}

