//
//  ImageLoadView.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/30.
//

import SwiftUI

private let url = URL(string: "https://pics0.baidu.com/feed/4ec2d5628535e5dd8494b24bc8dce2e7cf1b6288.jpeg?token=363b402a196cc00884211d87e419c863")!

struct ImageLoadView: View {
    var body: some View {
        
        NavigationView {
            List {
                NavigationLink(destination: SimpleExample(url: url) ) {
                    Text("简单例子")
                }
                NavigationLink(destination: WebImageDemo(url: url)) {
                    Text("网络加载")
                }
            }
            .navigationBarTitle("测试", displayMode: .inline)
            
        }
    }
}

struct ImageLoadView_Previews: PreviewProvider {
    static var previews: some View {
        ImageLoadView()
    }
}
