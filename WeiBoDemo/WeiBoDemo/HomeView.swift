//
//  HomeView.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/24.
//

import SwiftUI

struct HomeView: View {
    
//    init() {
//        // 隐藏默认的分割线
//        UITableView.appearance().separatorStyle = .none
//        // 去除点击cell，cell变灰
//        UITableViewCell.appearance().selectionStyle = .none
//    }
    
//    @EnvironmentObject var userData: UserData
    
    @State private var leftPercent: CGFloat = 0
    
    var body: some View {
        
        NavigationView {
            
            GeometryReader { geometry in
                
                HScrollViewController(pageWidth: geometry.size.width, contentSize: CGSize(width: geometry.size.width * 2, height: geometry.size.height), leftPercent: self.$leftPercent) {
                    
                    HStack {
                        PostListView(category: .recomend)
                            .frame(width: geometry.size.width)
                            .offset(x: 5)
                        
                        PostListView(category: .hot)
                            .frame(width: geometry.size.width)
                    }
                    
                    
                }
                // 忽略底部的安全区域
                .edgesIgnoringSafeArea(.bottom)
            }
            // 忽略底部的安全区域
            .edgesIgnoringSafeArea(.bottom)
            .frame(width: ScreenWidth)
            .toolbar(content: {
                        ToolbarItem(placement: .automatic) {
                            HomeNavigationBar(leftPercent: $leftPercent)
                        }
                    })
                    .navigationBarTitle("首页", displayMode: .inline)
            
            
        }
        // 适配ipad和iPhone一样
        .navigationViewStyle(StackNavigationViewStyle())
        
        
        
        
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .environmentObject(UserData())
    }
}


/*
 NavigationView {
     
     ScrollView(.horizontal, showsIndicators: false, content: {

         HStack {
             PostListView(category: .recomend)
                 .frame(width: ScreenWidth)
             PostListView(category: .hot)
                 .frame(width: ScreenWidth)
         }

     })
     // 忽略底部的安全区域
     .edgesIgnoringSafeArea(.bottom)
     .frame(width: ScreenWidth)
     .toolbar(content: {
                 ToolbarItem(placement: .automatic) {
                     HomeNavigationBar(leftPercent: 0)
                 }
             })
             .navigationBarTitle("首页", displayMode: .inline)
     
 }
 */
