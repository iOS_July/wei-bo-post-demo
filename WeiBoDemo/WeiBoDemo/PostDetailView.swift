//
//  PostDetailView.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/23.
//

import SwiftUI
import BBSwiftUIKit

struct PostDetailView: View {
    
    let post: Post
    
    var body: some View {
        
        BBTableView(0...10) { i in
            if i == 0 {
                PostCell(post: post)
            } else {
                
                HStack {
                    Text("评论\(i)")
                        .padding()
                    Spacer()
                }
            }
        }
        // 忽略底部的安全区域
        .edgesIgnoringSafeArea(.bottom)
        .navigationBarTitle("详情", displayMode: .inline)
        /*
        List {
            PostCell(post: post)
                .listRowInsets(EdgeInsets())
            
            
            ForEach(1...10, id: \.self) { i in
                Text("评论\(i)")
            }
        }
        .navigationBarTitle("详情", displayMode: .inline)
        */
        
    }
}

struct PostDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let userData = UserData()
        return PostDetailView(post: userData.recomendPostList.list[0]).environmentObject(userData)
    }
}
