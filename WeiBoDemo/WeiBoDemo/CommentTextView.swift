//
//  CommentTextView.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/26.
//

import SwiftUI

/*
 SwiftUI 里暂时没有，支持多行输出的view
 这里将UIKit的TextView，封装成SwiftUI的view
 */

struct CommentTextView: UIViewRepresentable {
    
    @Binding var text: String
    
    let beginEditingOnAppear: Bool
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.backgroundColor = .systemGray6
        view.font = .systemFont(ofSize: 18)
        view.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        view.delegate = context.coordinator
        view.text = text
        return view
    }
    
    
    func updateUIView(_ uiView: UITextView, context: Context) {
        if beginEditingOnAppear,//是否需要一进来就显示为编辑状态
           !context.coordinator.didBecomeFirstResponder,//还没成为第一响应者
           uiView.window != nil,//是否是加载了uiview,并且不为空
           !uiView.isFirstResponder//当前不是第一响应者
           {
            uiView.becomeFirstResponder()
            context.coordinator.didBecomeFirstResponder = true
        }
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        let parent: CommentTextView
        var didBecomeFirstResponder: Bool = false
        
        init(_ view: CommentTextView) {
            parent = view
        }
        
        func textViewDidChange(_ textView: UITextView) {
            parent.text = textView.text
        }
    }
}

struct CommentTextView_Previews: PreviewProvider {
    static var previews: some View {
        CommentTextView(text: .constant(""), beginEditingOnAppear: false)
    }
}
