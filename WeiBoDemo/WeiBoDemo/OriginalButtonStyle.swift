//
//  OriginalButtonStyle.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/31.
//

import SwiftUI


struct OriginalButtonStyle: ButtonStyle {
    // 遵守协议
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
    }
}
