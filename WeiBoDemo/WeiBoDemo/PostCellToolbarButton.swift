//
//  PostCellToolbarButton.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/23.
//

import SwiftUI

struct PostCellToolbarButton: View {
    /// 图片名称
    let image: String
    /// 内容
    let text: String
    /// 颜色
    let color: Color
    /// 指令事件
    let action: () -> Void // closure
    
    var body: some View {
        
        Button(action: action, label: {
            
            HStack (spacing: 5)
            {
                Image(systemName: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 18, height: 18)
                Text(text)
                    .font(.system(size: 15))
            }
            .foregroundColor(color)
        })
        // 只有点击按钮区域，按钮事件才响应
        .buttonStyle(BorderlessButtonStyle())
    }
}

struct PostCellToolbarButton_Previews: PreviewProvider {
    static var previews: some View {
        PostCellToolbarButton(image: "heart", text: "点赞", color: .red) {
            print("点赞")
        }
    }
}
