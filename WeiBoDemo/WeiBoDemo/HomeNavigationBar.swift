//
//  HomeNavigationBar.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/23.
//

import SwiftUI

private let klabelWidth: CGFloat = 60
private let kButtonHeight: CGFloat = 24

struct HomeNavigationBar: View {
    
    /// 当=0时，下划线在左边｜当=1时，下划线在右边
    @Binding var leftPercent: CGFloat
    
    let maxWidth: CGFloat = ScreenWidth
    
    var body: some View {
        
        HStack(alignment: .center) {
            
            Button(action: {
                print("点击 相机")
            }, label: {
                Image(systemName: "camera")
                    .resizable()
                    .scaledToFit()
                    .frame(width: kButtonHeight,height: kButtonHeight)
                    .padding(.horizontal, 15)
                    .foregroundColor(.black)
            })
            
            Spacer()
            
            GeometryReader { geometry in
                
                VStack (spacing: 3)
                {
                    HStack (spacing: 0)
                    {
                        
                        Text("推荐")
                            .bold()
                            .frame(width: klabelWidth,height: kButtonHeight)
                            .padding(.top, 5)
                            .opacity(Double(1-leftPercent*0.5))
                            .onTapGesture {
                                withAnimation {
                                    self.leftPercent = 0
                                }
                                
                            }
                        
                        Spacer()
                        
                        Text("热门")
                            .bold()
                            .frame(width: klabelWidth,height: kButtonHeight)
                            .padding(.top, 5)
                            .opacity(Double(0.5+leftPercent*0.5))
                            .onTapGesture {
                                withAnimation {
                                    self.leftPercent = 1
                                }
                            }
                        
                    }
                    .font(.system(size: 20))
                    
                    
                    //                    // 这里的x 用当leftPercent=0时，当leftPercent=1时来带入，就能理解了
                    RoundedRectangle(cornerRadius: 2)
                        .foregroundColor(.orange)
                        .offset(x: geometry.size.width * (self.leftPercent - 0.5) + klabelWidth * (0.5 - self.leftPercent))
                        .frame(width: 20, height: 4)
                    //                // 这里的x 用当leftPercent=0时，当leftPercent=1时来带入，就能理解了
                    //                    RoundedRectangle(cornerRadius: 2)
                    //                        .foregroundColor(.orange)
                    //                        .frame(width:20, height: 4)
                    //                        .offset(x: maxWidth * (self.leftPercent - 0.5) + klabelWidth * (0.5 - self.leftPercent))
                    
                }
                
                
                
            }
            .frame(width: 200,height: kButtonHeight*1.5)
            
            
            
            
            Spacer()
            
            Button(action: {
                print("点击 加号 按钮")
            }, label: {
                Image(systemName: "plus.circle.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(width: kButtonHeight,height: kButtonHeight)
                    .padding(.horizontal, 15)
                    .foregroundColor(.orange)
            })
        }
        .frame(width: maxWidth)
        
        
    }
}

struct HomeNavigationBar_Previews: PreviewProvider {
    static var previews: some View {
        HomeNavigationBar(leftPercent: .constant(0))
    }
}
