//
//  NetworkAPI.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/29.
//

import Foundation

class NetworkAPI {
    
    
    /// 解析数据 <T> 代表范型，<T: Decodable>代表该范型遵循Decodable协议
    private static func parseData<T: Decodable>(_ data: Data) -> Result<T, Error> {
        guard let decodedData = try? JSONDecoder().decode(T.self, from: data) else {
            let error = NSError(domain: "NetworkAPIError", code: 0, userInfo: [NSLocalizedDescriptionKey: "无法解析数据"])
            return .failure(error)
        }
        
        return .success(decodedData)
    }
    
    
    /// 获取推荐列表的数据
    static func recommendPostList(completion: @escaping (Result<PostList, Error>) -> Void ) {
        NetWorkManager.shared.requestGet(path: "PostListData_recommend_1.json", parameters: nil) { result in
            switch result {
            case let .success(data):
                let parseResult: Result<PostList, Error> = self.parseData(data)
                completion(parseResult)
            case let .failure(error): completion(.failure(error))
            }
            
        }
    }
    
    
    /// 获取热门列表的数据
    static func hotPostList(completion: @escaping (Result<PostList, Error>) -> Void ) {
        NetWorkManager.shared.requestGet(path: "PostListData_hot_1.json", parameters: nil) { result in
            switch result {
            case let .success(data):
                let parseResult: Result<PostList, Error> = self.parseData(data)
                completion(parseResult)
            case let .failure(error): completion(.failure(error))
            }
            
        }
    }
    
    
    /// 发一条微博
    static func createPost(text: String, completion: @escaping (Result<Post, Error>) -> Void) {
        NetWorkManager.shared.requstPost(path: "createpost", parameters: ["text": text]) { result in
            switch result {
            case let .success(data):
                let parseResult: Result<Post, Error> = self.parseData(data)
                completion(parseResult)
            case let .failure(error): completion(.failure(error))
            }
        }
    }
    
    
    /*
    /// 获取推荐列表的数据
    static func recommendPostList(completion: @escaping (Result<PostList, Error>) -> Void ) {
        NetWorkManager.shared.requestGet(path: "PostListData_recommend_1.json", parameters: nil) { result in
            switch result {
            case let .success(data):
                // Handle data
                guard let list = try? JSONDecoder().decode(PostList.self, from: data) else {
                    let error = NSError(domain: "NetworkAPIError", code: 0, userInfo: [NSLocalizedDescriptionKey: "无法解析数据"])
                    //NSLocalizedDescriptionKey | error.localizedDescription
                    completion(.failure(error))
                    return
                }
                completion(.success(list))
            case let .failure(error): completion(.failure(error))
            }
            
        }
    }
    
    
    /// 获取热门列表的数据
    static func hotPostList(completion: @escaping (Result<PostList, Error>) -> Void ) {
        NetWorkManager.shared.requestGet(path: "PostListData_hot_1.json", parameters: nil) { result in
            switch result {
            case let .success(data):
                // Handle data
                guard let list = try? JSONDecoder().decode(PostList.self, from: data) else {
                    let error = NSError(domain: "NetworkAPIError", code: 0, userInfo: [NSLocalizedDescriptionKey: "无法解析数据"])
                    //NSLocalizedDescriptionKey | error.localizedDescription
                    completion(.failure(error))
                    return
                }
                completion(.success(list))
            case let .failure(error): completion(.failure(error))
            }
            
        }
    }
    */
    
}
