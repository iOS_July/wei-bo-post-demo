//
//  PostCell.swift
//  WeiBoDemo
//
//  Created by chaowen deng on 2021/3/23.
//

import SwiftUI

struct PostCell: View {
    
    let post: Post
    
    // 点赞 更新数据，需要更新的是环境变量，且环境变量内的属性，需要用@Published修饰才行
    @EnvironmentObject var userData: UserData
    // 同样，显示的数据，也应该从userData里去查找到来显示
    var bindingPost: Post {
        userData.post(forId: post.id)!
    }

    // 模态 推出评论
    @State var presentComment: Bool = false
    
    var body: some View {
        // 这里是写了一个局部变量
        var post = bindingPost
        
        VStack(alignment: .leading, spacing: 10) {
            
            
            HStack(spacing: 5) {
//                Image(uiImage: UIImage(named: post.avatar)!)
                post.avatarImage
                    //可收缩
                    .resizable()
                    //收缩系数 按比例
                    .scaledToFill()
                    .frame(width: 50, height: 50)
                    .clipShape(Circle())
                    .overlay(
                        PostCellVipBadge(vip: post.vip)
                            .offset(x: 16, y: 16)
                    )
                
                VStack(alignment: .leading, spacing: 5.0) {
                    Text(post.name)
                        .font(Font.system(size: 16))
                        .foregroundColor(Color(red: 242/255, green: 99/255, blue: 4/255))
                        .lineLimit(1)
                    
                    Text(post.date)
                        .foregroundColor(.gray)
                        .font(.system(size: 11))
                }
                .padding(.leading, 10)
                
                Spacer()
                
                if !post.isFollowed {
                    Button(action: {
                        post.isFollowed = true
                        self.userData.update(post)
                    }, label: {
                        Text("关注")
                            .foregroundColor(.orange)
                            .font(.system(size: 14))
                            .frame(width: 50, height: 26)
                            //圆角矩形 一个view上再加一个view
                            .overlay(
                                RoundedRectangle(cornerRadius: 26/2)
                                    //只需要边框，描边
                                    .stroke(Color.orange, lineWidth: 1)
                            )
                        
                    })
                    .buttonStyle(BorderlessButtonStyle())
                }
                
                
                
            }
            .padding(.horizontal, 15)
            
            Text(post.text)
                .font(.system(size: 17))
                .padding(.horizontal, 15)
//                .frame(width: ScreenWidth-30)
                
            
            
            if !post.images.isEmpty {
                PostImageCell(images: post.images, width: ScreenWidth-30)
                    .padding(.horizontal, 15)
            }
            
            // 分割线
            Divider()
            
            HStack (spacing: 0)
            {
                Spacer()
                
                PostCellToolbarButton(image: "message", text: post.commentCountText, color: .black) {
                    self.presentComment = true
                }
                .sheet(isPresented: $presentComment, content: {
                    CommentInputView(post: post).environmentObject(self.userData)
                })
                Spacer()
                PostCellToolbarButton(image: post.isLiked ? "heart.fill" : "heart", text: post.likeCountText, color: post.isLiked ?  .red : .black) {
                    if post.isLiked {
                        post.isLiked = false
                        post.likeCount -= 1
                    } else {
                        post.isLiked = true
                        post.likeCount += 1
                    }
                    self.userData.update(post)
                }
                Spacer()
            }
            .padding(.horizontal, 15)
            
            Rectangle()
                .padding(.horizontal, -15)
                .frame(height: 10)
                .foregroundColor(Color(red: 240/255, green: 240/255, blue: 240/255))
        }
        .padding(.top, 15)

        
        
    }
    
}

struct PostCell_Previews: PreviewProvider {
    static var previews: some View {
        let userData = UserData()
        return PostCell(post: userData.recomendPostList.list[0]).environmentObject(userData)
    }
}
